import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class View extends JFrame {

	private JPanel mainPanel, buttonsPanel;
	private JLabel menuTitle;
	private JLabel menuPrompt;
	private JButton button;
	private ActionListener actionListener;

	public View() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 725, 395);
		setTitle("Menu");
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mainPanel.setLayout(new BorderLayout(0, 0));
		setContentPane(mainPanel);

		JPanel header = new JPanel();
		header.setBackground(new Color(225, 246, 255));
		header.setPreferredSize(new Dimension(100, 120));
		header.setLayout(null);
		mainPanel.add(header, BorderLayout.NORTH);

		menuTitle = new JLabel();
		menuTitle.setText("Hello");
		menuTitle.setFont(new Font("Tahoma", Font.PLAIN, 35));
		menuTitle.setForeground(Color.black);
		menuTitle.setHorizontalAlignment(SwingConstants.CENTER);
		header.add(menuTitle);
		menuTitle.setBounds(0, 0, 715, 74);

		menuPrompt = new JLabel("level 2");
		menuPrompt.setForeground(Color.WHITE);
		menuPrompt.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 14));
		menuPrompt.setBounds(10, 80, 380, 13);
		header.add(menuPrompt);

		buttonsPanel = new JPanel();
		buttonsPanel.setBackground(new Color(225, 246, 255));
		buttonsPanel.setBorder(new EmptyBorder(9, 5, 0, 5));
		buttonsPanel.setLayout(new GridLayout(3, 3, 15, 15));
		mainPanel.add(buttonsPanel, BorderLayout.CENTER);
	}

	public void addMenu(List<String> menu) {
		buttonsPanel.removeAll();
		menuTitle.setText(menu.get(0));
		menuPrompt.setText(menu.get(1));
		for (int i = 2; i < menu.size(); i++) {
			button = new JButton(menu.get(i));
			button.setFont(new Font("Tahoma", Font.BOLD, 15));
			button.setBackground(new Color(243, 243, 243));
			button.addActionListener(actionListener);
			buttonsPanel.add(button);
		}
		buttonsPanel.revalidate();
		buttonsPanel.repaint();
	}
	
	public void addActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;
	}
}
