import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
	private Model model;
	private View view;
	
	public Controller(Model menuModel, View menuView) {
		super();
		this.model = menuModel;
		this.view = menuView;
		
		this.view.addActionListener(new ButtonClickListener());
	}
	
	public void addMenu(int menu) {
		view.addMenu(model.getMenu(menu));
	}
	
	public void setPlayerName(String playerName) {
		model.setPlayerName(playerName);
	}
	
	public void setViewVisible() {
		view.setVisible(true);
	}
	
	class ButtonClickListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Aardvark.option = model.getOption(e.getActionCommand());
		}
		
	}
	
}
